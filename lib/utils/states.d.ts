import { ReferenceSource } from "../helper/status";
export interface State {
    value: number;
    text: string;
}
export declare const STATES: State[];
export declare const STATE_SOURCE: ReferenceSource;
