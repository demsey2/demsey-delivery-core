"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var generators_1 = require("./generators");
exports.MONTHS = [
    "January",
    "February",
    "March",
    "April",
    "May",
    "June",
    "July",
    "August",
    "September",
    "October",
    "November",
    "December"
];
exports.MONTHS_SOURCE = {
    getAll: function () {
        return exports.DATE.MONTHS;
    }
};
exports.DATE = {
    DAYS: generators_1.days(),
    MONTHS: exports.MONTHS.map(function (month, index) {
        return { text: month, value: ++index };
    }),
    YEARS: generators_1.years(1900)
};
