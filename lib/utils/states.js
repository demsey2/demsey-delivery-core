"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.STATES = [
    { text: "New South Wales", value: 1 },
    { text: "Queensland", value: 2 },
    { text: "South Australia", value: 3 },
    { text: "Tasmania", value: 4 },
    { text: "Victoria", value: 5 },
    { text: "Western Australia", value: 6 }
];
exports.STATE_SOURCE = {
    getAll: function () {
        return exports.STATES;
    }
};
