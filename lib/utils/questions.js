"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.QUESTIONS = {
    YESNO_OPTIONS: [
        { text: "Yes", value: "1" },
        { text: "No", value: "0" }
    ]
};
