"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.TYPE = {
    PROJECT: 1,
    STANDARD: 2
};
exports.TYPES = [
    { id: exports.TYPE.PROJECT, name: "Custom delivery", days: 0 },
    { id: exports.TYPE.STANDARD, name: "Standard delivery", days: 0 }
];
