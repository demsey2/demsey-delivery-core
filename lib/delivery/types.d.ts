export declare const TYPE: {
    PROJECT: number;
    STANDARD: number;
};
export interface Type {
    id: number;
    name: string;
    days: number;
}
export declare const TYPES: Type[];
