"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var _ = require("lodash");
exports.DAY = {
    PROJECT: 1,
    STANDARD: 2
};
exports.DAYS = [
    { name: 'Monday', bit: 1, id: 1 },
    { name: 'Tuesday', bit: 2, id: 2 },
    { name: 'Wednesday', bit: 4, id: 3 },
    { name: 'Thursday', bit: 8, id: 4 },
    { name: 'Friday', bit: 16, id: 5 },
    { name: 'Saturday', bit: 32, id: 6 },
    { name: 'Sunday', bit: 64, id: 0 }
];
exports.findDaysByBit = function (bit) {
    return _.filter(exports.DAYS, function (day) { return (day.bit & bit) > 0; });
};
exports.checkIfBitMatchesDayId = function (id, bit) {
    return (bit & _.find(exports.DAYS, function (day) { return day.id === id; }).bit) === 0;
};
