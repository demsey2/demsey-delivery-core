export declare const DAY: {
    PROJECT: number;
    STANDARD: number;
};
export interface Day {
    id: number;
    name: string;
    bit: number;
}
export declare const DAYS: Day[];
export declare const findDaysByBit: (bit: number) => Day[];
export declare const checkIfBitMatchesDayId: (id: number, bit: number) => Boolean;
