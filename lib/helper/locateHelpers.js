"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var _ = require("lodash");
var getDevices = function (collection, location, where) {
    return new Promise(function (resolve, reject) {
        collection.aggregate([
            {
                $geoNear: {
                    near: location,
                    distanceField: 'jobDistance',
                    spherical: true,
                    query: where,
                    distanceMultiplier: 0.001
                }
            },
            { '$redact': {
                    '$cond': {
                        'if': { '$lt': ['$jobDistance', '$distance'] },
                        'then': '$$KEEP',
                        'else': '$$PRUNE'
                    }
                } },
            { $project: { _id: 0, devices: '$devices.pushId' } },
            { $unwind: '$devices' },
            { $unwind: '$devices' },
            { $group: { _id: 'devices', results: { $addToSet: '$devices' } } },
            { $project: { _id: 0, results: 1 } }
        ], function (err, data) {
            if (err)
                return reject(err);
            return resolve(_.get(data, '[0].results'));
        });
    });
};
var combinedResults = function (helpers) {
    return _.union(helpers[0], helpers[1]);
};
exports.default = function (context) {
    var job = context.job;
    context.whereClause = { notification: true };
    context.coordinatesFrom = job.locFrom;
    context.coordinatesTo = job.locTo;
    if (context.insured === true) {
        context.whereClause.insured = true;
    }
    var fromDevices = getDevices(context.db.collection('helper'), job.locFrom, context.whereClause);
    var toDevices = getDevices(context.db.collection('helper'), job.locTo, context.whereClause);
    return Promise.all([fromDevices, toDevices])
        .then(combinedResults);
};
