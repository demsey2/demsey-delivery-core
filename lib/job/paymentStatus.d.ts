export declare const PAYMENT_STATUS: {
    AUTHORISED: number;
    CAPTURED: number;
    FAILED: number;
    REFUNDED: number;
    CASH: number;
};
export interface PaymentStatus {
    value: number;
    text: string;
}
export declare const PAYMENT_STATUSES: PaymentStatus[];
