"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.PAYMENT_STATUS = {
    AUTHORISED: 1,
    CAPTURED: 2,
    FAILED: 3,
    REFUNDED: 4,
    CASH: 5
};
exports.PAYMENT_STATUSES = [
    { value: exports.PAYMENT_STATUS.AUTHORISED, text: "Authorised" },
    { value: exports.PAYMENT_STATUS.CAPTURED, text: "Captured" },
    { value: exports.PAYMENT_STATUS.FAILED, text: "Failed" },
    { value: exports.PAYMENT_STATUS.REFUNDED, text: "Refunded" },
    { value: exports.PAYMENT_STATUS.CASH, text: "Cash" }
];
