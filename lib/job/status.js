"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.STATUS = {
    QUOTE: 1,
    BOOKED: 2,
    CANCELLED: 3,
    ACCEPTED: 4,
    COMPLETED: 5,
    REFUNDED: 6,
    PAID: 7
};
exports.STATUSES = [
    { value: exports.STATUS.QUOTE, text: "Quote" },
    { value: exports.STATUS.BOOKED, text: "Booked" },
    { value: exports.STATUS.CANCELLED, text: "Cancelled" },
    { value: exports.STATUS.ACCEPTED, text: "Accepted" },
    { value: exports.STATUS.COMPLETED, text: "Completed" },
    { value: exports.STATUS.REFUNDED, text: "Refunded" },
    { value: exports.STATUS.PAID, text: "Paid" }
];
