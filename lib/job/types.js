"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.TYPE = {
    FURNITURE: 1,
    SM_HOUSE_APP: 2,
    RUBBISH_REMOVAL: 3
};
exports.TYPES = [
    { id: exports.TYPE.FURNITURE, label: "Furniture pickup/delivery" },
    { id: exports.TYPE.SM_HOUSE_APP, label: "Small house/apartment move" },
    { id: exports.TYPE.RUBBISH_REMOVAL, label: "Rubbish and waste disposal" }
];
