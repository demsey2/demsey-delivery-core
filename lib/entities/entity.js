"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var _ = require("lodash");
var Entity = (function () {
    function Entity(descriptor, values) {
        if (values === void 0) { values = {}; }
        var _this = this;
        var fields = descriptor.fields;
        _.forOwn(fields, function (field) {
            _this[field.name] = field.parse(values[field.name]) || field.defaultValue;
        });
        this.descriptor = descriptor;
    }
    Entity.prototype.serialize = function () {
        var _this = this;
        var output = {};
        this.descriptor.fields.forEach(function (field) {
            output[field.name] = field.serialize(_this[field.name]);
        });
        return output;
    };
    Entity.prototype.getField = function (name) {
        return _.find(this.descriptor.fields, function (field) { return field.name === name; });
    };
    Entity.prototype.getRules = function (name) {
        return _.find(this.descriptor.fields, function (field) { return field.name === name; }).rules;
    };
    return Entity;
}());
exports.Entity = Entity;
;
exports.createEntity = function (descriptor, data) {
    return new Entity(descriptor, data);
};
