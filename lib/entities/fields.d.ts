export declare class Field {
    name: string;
    type: string;
    rules: string;
    defaultValue: any;
    basic: boolean;
    friendlyName: string;
    constructor(name: string, type: string, rules: string, defaultValue: any, basic: boolean, friendlyName: any);
    parse(value: any): any;
    serialize(value: any): any;
}
export declare class ReferenceField extends Field {
    referenceType: string;
    constructor(name: string, referenceType: string, rules: string, defaultValue: any, basic: boolean, friendlyName: any);
    parse(value: any): any;
    serialize(value: any): any;
}
export declare class ReferenceSourceField extends ReferenceField {
    referenceSource: any;
    constructor(name: string, referenceType: string, rules: string, defaultValue: any, basic: boolean, friendlyName: any, source: any);
    parse(value: any): any;
}
export declare const buildField: (name: string, type: string, rules?: string, defaultValue?: any, basic?: boolean, friendlyName?: any) => Field;
export declare const buildReferenceField: (name: string, referenceType: string, rules?: string, defaultValue?: any, basic?: boolean, friendlyName?: any) => ReferenceField;
export declare const buildReferenceFieldWithSource: (name: string, referenceType: string, source: any, rules?: string, defaultValue?: any, basic?: boolean, friendlyName?: any) => ReferenceSourceField;
