import * as _ from "lodash";

export class EntityDescriptor {
  public fields: any[];

  constructor (data: any) {
    this.fields = data.fields;
    if (data.methods) {
      _.assign(this, data.methods);
    }
  }

  getRules () {
    let rules: any = {};
    _.forEach(this.fields, (field) => {
      if (!field.basic && field.rules) {
        rules[field.name] = field.rules;
      }
    });
    return rules;
  }

  getBasicRules () {
    let rules: any = {};
    _.forEach(this.fields, (field) => {
      if (field.basic) {
        rules[field.name] = field.rules;
      }
    });
    return rules;
  }

  getFieldsFriendlyNames () {
    let names: any = {};
    _.forEach(this.fields, (field) => {
      if (field.friendlyName) {
        names[field.name] = field.friendlyName;
      }
    });
    return names;
  }
}
