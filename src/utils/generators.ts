export interface IMapGeneric { text: String; value: number; }

export const years = (min = 1990): IMapGeneric[] => {
  let years: IMapGeneric[] = [];
  let current = new Date().getFullYear();
  while (current >= min) {
    years.push({
      text: current.toString(),
      value: current
    });
    current--;
  }
  return years;
};

export const futureYears = (max = 10): IMapGeneric[] => {
  let years: IMapGeneric[] = [];
  let current = new Date().getFullYear();
  let maxDate = new Date().getFullYear();
  while (current <= maxDate + max) {
    years.push({
      text: current.toString(),
      value: current
    });
    current++;
  }
  return years;
};


export const days = (): IMapGeneric[] => {
  let days: IMapGeneric[] = [];
  let start = 1;
  while (start <= 31) {
    days.push({
      text: start.toString(),
      value: start
    });
    start++;
  }
  return days;
};
