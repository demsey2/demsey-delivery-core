export const PAYMENT_STATUS = {
  AUTHORISED: 1,
  CAPTURED: 2,
  FAILED: 3,
  REFUNDED: 4,
  CASH: 5
};

export interface PaymentStatus {
  value: number;
  text: string;
}

export const PAYMENT_STATUSES: PaymentStatus[] = [
  { value: PAYMENT_STATUS.AUTHORISED, text: "Authorised" },
  { value: PAYMENT_STATUS.CAPTURED, text: "Captured" },
  { value: PAYMENT_STATUS.FAILED, text: "Failed" },
  { value: PAYMENT_STATUS.REFUNDED, text: "Refunded" },
  { value: PAYMENT_STATUS.CASH, text: "Cash" }
];
