import * as _ from 'lodash';

export const DAY = {
  PROJECT: 1,
  STANDARD: 2
};

export interface Day {
  id: number;
  name: string;
  bit: number;
}

export const DAYS: Day[] = [
  { name: 'Monday', bit: 1, id: 1 },
  { name: 'Tuesday', bit: 2, id: 2 },
  { name: 'Wednesday', bit: 4, id: 3},
  { name: 'Thursday', bit: 8, id: 4 },
  { name: 'Friday', bit: 16, id: 5 },
  { name: 'Saturday', bit: 32, id: 6 },
  { name: 'Sunday', bit: 64, id: 0 }
];

export const findDaysByBit = (bit: number): Day[] => {
  return _.filter(DAYS, (day: Day) => (day.bit & bit) > 0)
}

export const checkIfBitMatchesDayId = (id: number, bit: number): Boolean => {
  return (bit & _.find(DAYS, (day: Day) => day.id === id).bit) === 0;
}
